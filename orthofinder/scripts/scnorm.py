# -*- coding: utf-8 -*-
#
# Copyright 2014 David Emms
#
# This program (OrthoFinder) is distributed under the terms of the GNU General Public License v3
#
#    This program is free software: you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License
#    along with this program.  If not, see <http://www.gnu.org/licenses/>.
#  
#  When publishing work that uses OrthoFinder please cite:
#      Emms, D.M. and Kelly, S. (2015) OrthoFinder: solving fundamental biases in whole genome comparisons dramatically 
#      improves orthogroup inference accuracy, Genome Biology 16:157
#
# For any enquiries send an email to David Emms
# david_emms@hotmail.com 


from scipy.optimize import curve_fit            # install
import scipy.sparse as sparse                   # install
import numpy as np
import matplotlib as mpl                        # install
mpl.use('Agg')
import matplotlib.pyplot as plt                 # install
import sys


counter=0
f = []

"""
scnorm
-------------------------------------------------------------------------------
"""
class scnorm:
    @staticmethod
    def loglinear(x, a, b):
        # return the loglinear function
        return a*np.log10(x)+b

    @staticmethod
    def GetLengthArraysForMatrix(m, len_i, len_j):
        # return (I,j) positions non zeros values for the m matrix
        I, J = m.nonzero()
        scores = [v for row in m.data for v in row]     # use fact that it's lil
        Li = np.array(len_i[I])
        Lj = np.array(len_j[J])
        return Li, Lj, scores

    @staticmethod
    def GetTopPercentileOfScores(L, S, percentileToKeep):
        # Get the top x% of hits at each length
        nScores = len(S)
        t_sort = sorted(zip(L, range(nScores)))
        indices = [j for i, j in t_sort]
        s_sorted = [S[i] for i in indices]
        l_sorted = [L[i] for i in indices]
        if nScores < 100:
            # then we can't split them into bins, return all for fitting
            return l_sorted, s_sorted
        nInBins = 1000 if nScores > 5000 else (200 if nScores > 1000 else 20)
        nBins, remainder = divmod(nScores, nInBins)
        topScores = []
        topLengths = []
        for i in range(nBins):
            first = i*nInBins
            last = min((i+1)*nInBins-1, nScores - 1)
            theseLengths = l_sorted[first:last+1]
            theseScores = s_sorted[first:last+1]
            cutOff = np.percentile(theseScores, percentileToKeep)
            lengthsToKeep = [thisL for thisL, thisScore in zip(theseLengths, theseScores) if thisScore >= cutOff]
            topLengths.extend(lengthsToKeep)
            topScores.extend([thisScore for thisL, thisScore in zip(theseLengths, theseScores) if thisScore >= cutOff])
        return topLengths, topScores

    @staticmethod
    def CalculateFittingParameters(Lf, S, qNormalizeCurve):
        # curve_fit doc.: https://docs.scipy.org/doc/scipy/reference/generated/scipy.optimize.curve_fit.html
        Slog = np.log10(S)
        pars, covar =  curve_fit(scnorm.loglinear, Lf, Slog)
        if qNormalizeCurve:
            scnorm.DrawLogLinear(pars, Lf)
        return pars

    @staticmethod
    def NormaliseScoresByLogLengthProduct(b, Lq, Lh, params):
        rangeq = list(range(len(Lq)))
        rangeh = list(range(len(Lh)))
        li_vals = Lq**(-params[0])
        lj_vals = Lh**(-params[0])
        # l{i,j}_matrix are built using l{i,j}_vals data using (range{q,h},range{q,h}) positions
        li_matrix = sparse.csr_matrix((li_vals, (rangeq, rangeq)))
        lj_matrix = sparse.csr_matrix((lj_vals, (rangeh, rangeh)))
        # lil_matrix named as Bij in ProcessBlastHits
        return sparse.lil_matrix(10**(-params[1]) * li_matrix * b * lj_matrix)

    @staticmethod
    def DrawLogLinear(params, x):
        global counter
        global f
        nbvals=70000
        maxBS=7000
        minBS=0
        x = np.linspace(minBS, maxBS, nbvals)
        print("Drawing fitting curve")
        f.append(plt.plot(x, scnorm.loglinear(x, *params), label='species '+str(counter)))
        plt.legend()
        plt.xlabel('Bits Scores')
        plt.ylabel('Normalized Scores')
        plt.title("Normalization fitting curve for species")
        plt.grid(True)
        plt.savefig('fitting_curve.png')
        counter+=1


    @staticmethod
    def NormalizedScores(iSpeciesOpen, jSpeciesOpen, iQ, iH, sep, B, rows):
        """
        This function retrieve the Normalization Score
        from Matrix B from (Seq iQ, Seq iH)
        and return the csv rows modified
        by adding normalization score as a new column
        """
        newrows = []
        for row in rows:
            # Get hit and query IDs
            try:
                sequence1ID = int(row[iQ].split(sep, 2)[1])
                sequence2ID = int(row[iH].split(sep, 2)[1])
            except (IndexError, ValueError):
                sys.stderr.write("\nERROR: Query or hit sequence ID in BLAST results file was missing or incorrectly formatted.\n")
                raise
            try:
                newscore = B[sequence1ID, sequence2ID]
                row.append(newscore)
            except IndexError as e:
                sys.stderr.write("\nERROR: Matrix index on Blast species %d %d, sequences B[%d, %d] : %s \n" % (iSpeciesOpen, jSpeciesOpen, sequence1ID, sequence2ID, e))
                pass
            newrows.append(row)
        return newrows

    @staticmethod
    def FixZeroNormalizedScores(iSpeciesOpen, rows):
        """
        This function retrieve the Normalization Score
        from current csv file (with Seq iQ, Seq iH)
        If it is 0.0 (self-hits), it will try to retrieve
        a modified value from the log10 fitting curve
        """
        selfHitNormBlastScore = "0.0"
        NormBS = selfHitNormBlastScore
        newrows = []
        # retrieve value from plot https://code-examples.net/en/q/8863d1
        # for debug purpose
        #print(repr(f))
        #print(repr(f[iSpeciesOpen]))
        all_plots = f[iSpeciesOpen][0].get_xydata()
        #print(repr(all_plots))
        error_margin = 0.2
        #line = 0
        print("Compute back 0.0 normalized score for self-hits for species %d %d" % (iSpeciesOpen, iSpeciesOpen))
        print("Error margin is %f" % error_margin)
        for row in rows:
            #try:
            if row[12] == selfHitNormBlastScore:
                #try:
                BS = float(row[11])
                for index, value in all_plots:
                    if BS-error_margin <= index <= BS+error_margin:
                        NormBS = float(value)
                        break
                row[12] = NormBS
                #except (IndexError, ValueError):
                #    sys.stderr.write("\nERROR: 12th field in BLAST results file line should be the bit-score for the hit\n")
            #except IndexError as e:
            #    sys.stderr.write("\nERROR: csv values on Blast species %d %d file, line %d : %s \n" % (iSpeciesOpen, iSpeciesOpen, line, e))
            #    pass
            newrows.append(row)
            #line+=1
        return newrows
